import 'package:flutter/material.dart';

import './start.dart';
import './quiz.dart';
import './result.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool _startedQuiz = false;

  var _questionIndex = 0;

  final _questions = const [
    {
      'question': 'Qual é sua cor favorita?',
      'answers': ['Vermelho', 'Azul', 'Verde']
    },
    {
      'question': 'Qual sua faixa de idade?',
      'answers': ['10 a 25 anos', '25 a 40 anos', '40 a 60 anos']
    },
    {
      'question': 'Qual seu Framework Javascript Favorito?',
      'answers': ['React', 'Vue', 'Svelte']
    },
  ];

  List _results = [];

  void _startQuiz() {
    setState(() {
      _startedQuiz = true;
    });
  }

  void _responderPergunta(String resposta) {
    if (_questionIndex < _questions.length) {
      setState(() {
        _results.add(resposta);
        _questionIndex++;
      });
    }
  }

  void _restartQuiz() {
    setState(() {
      _questionIndex = 0;
      _results = [];
    });
  }

  Widget build(BuildContext context) {
    var startedQuiz = !_startedQuiz ? Start(_startQuiz) : (_questionIndex < _questions.length
            ? Quiz(
                questions: _questions,
                questionIndex: _questionIndex,
                responderPergunta: _responderPergunta,
              )
            : Result(
                results: _results,
                questions: _questions,
                restartQuiz: _restartQuiz,
              ));

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Organizze'),
        ),
        body: startedQuiz,
      ),
    );
  }
}
