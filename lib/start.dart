import 'package:flutter/material.dart';

class Start extends StatelessWidget {
  final Function startQuiz;

  Start(this.startQuiz);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Container(
        child: Column(
          children: [
            Text(
              'Bem vindo ao Quiz de perguntas e respostas mais badalado da internet!',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 30,
                color: Colors.blue,
              ),
            ),
            RaisedButton(
              color: Colors.blue,
              textColor: Colors.white,
              child: Text('Iniciar Quiz'),
              onPressed: startQuiz,
            ),
          ],
        ),
      ),
    );
  }
}
