import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final List results;
  final List<Map<String, Object>> questions;
  final Function restartQuiz;

  List resultsMap = [];

  Result({this.results, this.questions, this.restartQuiz});

  @override
  Widget build(BuildContext context) {
    for (var i = 0; i < questions.length; i++) {
      resultsMap.add(
        {
          'question': questions[i]['question'],
          'answer': results[i],
        },
      );
    }

    return Container(
      width: double.infinity,
      child: Column(
        children: [
          ...resultsMap.map((r) {
            return Container(
              width: double.infinity,
              margin: EdgeInsets.all(10),
              child: Column(
                children: [
                  Container(
                    width: double.infinity,
                    child: Text(
                      r['question'],
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 20,
                      ),
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    child: Text(
                      'R: ' + r['answer'],
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                      ),
                    ),
                  ),
                ],
              ),
            );
          }).toList(),
          RaisedButton(
            color: Colors.blue,
            textColor: Colors.white,
            child: Text('Reiniciar Quiz'),
            onPressed: restartQuiz,
          ),
        ],
      ),
    );
  }
}
